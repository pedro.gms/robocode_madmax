package pedro;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Robot_MadMax - a robot by (Pedro Guimarães)
 */
public class Robot_MadMax extends AdvancedRobot
{
	boolean movingForward; //define condição para Robot_MadMax seguir em frente
	private byte moveDirection = 1;
	
	public void run() {

		setColors(Color.black,Color.red,Color.black); // body,gun,radar

		// Robot main loop
		while(true) {

			setAhead(100);
			setAdjustGunForRobotTurn(true); //separa movimento do canhão do movimento do Robot_MadMax
			setTurnRight(180); // vira Robot_MadMax 180º para direita
			setTurnGunLeft(90);// vira canhão 90º para a esquerda
			setBack(100);
			
			execute();
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		/**
	 	* lógica para cálculo da mira do canhão:
		* encontra a diferença entre a direção do Robot_MadMax e a direção do seu canhão
		* define mira!
	 	*/
		setTurnGunLeft(getHeading() - getGunHeading() + e.getBearing());
		setFire(Math.min(400 / e.getDistance(), 3));
		


	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
	
		setBack(100);
		setTurnLeft(180);
	}
	
	/**
	 * onHitRobot: What to do when you hit a robot
	 */
	public void onHitRobot(HitRobotEvent e) { 
		if (getVelocity() == 0)
			moveDirection *= -1;
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		reverseDirection();
	}
	
	/**
	 * reverseDirection: ir em frente ou ir para trás (vice versa)
	 */
	public void reverseDirection() {
		if (movingForward) {
			setBack(1000);
			movingForward = false;
		} else {
			setAhead(1000);
			movingForward = true;
		}
	}
}
