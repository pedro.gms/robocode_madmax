# Robot_MadMax

Robô criado para o Solutis Robot Arena 2020, através da plataforma Robocode.

## Implementação

O método principal run() do robô é implementado da seguinte forma:

```bash
setAhead(100);
setAdjustGunForRobotTurn(true); 
setTurnRight(180);
setTurnGunLeft(90);
setBack(100);
```

Onde setAhead(100) faz o robô caminhar 100 pixels para frente, o método setAdjustGunForRobotTurn() separa movimento do canhão do movimento do Robot_MadMax (setTurnGunLeft(90)), setTurnRight(180) aplica a estratégia de movimentação em círculos do robô e setBack(100) faz o Robot_MadMax andar 100 pixels para trás.

## onScannedRobot()

```java
setTurnGunLeft(getHeading() - getGunHeading() + e.getBearing());
setFire(Math.min(400 / e.getDistance(), 3));
```
O método setTurnGunLeft() calcula a diferença entre o ângulo do tanque Robot_MadMax e a direção do seu canhão.
Já o método setFire(Math.min(400 / e.getDistance(), 3)) faz o tiro do canhão relacionando o poder do disparo com a distância do inimigo.

## onHitByBullet()

```java
setBack(100);
setTurnLeft(180);
```
Movimentação básica para trás e para esquerda ao ser atingido por um disparo inimigo.

## onHitRobot()

```java
if (getVelocity() == 0)
	moveDirection *= -1;
```
Condicional para, caso Robot_MadMax colida com outro robô, ou fique preso nessa colisão, a direção dele seja alterada.

## onHitWall()

```java
	public void reverseDirection() {
		if (movingForward) {
			setBack(1000);
			movingForward = false;
		} else {
			setAhead(1000);
			movingForward = true;
		}
	}setTurnLeft(180);
```
O método onHitWall() faz a chamada de outro método: reverseDirection(). Este serve para reverter a direção do robô, caso o mesmo se choque contra uma parede.

